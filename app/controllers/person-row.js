import Ember from 'ember';

export default Ember.ObjectController.extend({
  nameAndTitle: function() {
    return "<strong>" + this.get('name') + "</strong> - " + this.get('title');
  }.property('name', 'title'),

  isSoldierActive: function() {     
    return this.get('isActive') ? "YES" : "NO";
  }.property('isActive'),

  actions: {
    didClickSoldier: function() {
      this.toggleProperty('isActive');
    }
  }
});
