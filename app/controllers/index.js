import Ember from 'ember';

export default Ember.ArrayController.extend({
  showActiveOnly: false,

  numSoldiers: function() {
    return this.get('length');
  }.property('length'),

  numActiveSoldiers: function() {
    var active = this.filterBy('isActive', true);

    return active.get('length');
  }.property('@each.isActive'),

  filteredSoldiers: Ember.computed.filter('@this', function(item) {
    return (!this.get('showActiveOnly') || item.isActive);
  }).property('@this', 'showActiveOnly'),

  actions: {
    addSoldier: function(soldier) {
      this.addObject(soldier);
    }

  }
});
