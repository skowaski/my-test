import Ember from 'ember';

export default Ember.Component.extend({
  isActive: true,
  name: '',
  title: '',     

  actions: {
    didClickAddButton: function() {
      var newPerson = {
        name: this.get('name'),
        title: this.get('title'),
        isActive: this.get('isActive')
      };
      this.sendAction('addClicked', newPerson);
    }
  }
});
