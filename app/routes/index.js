import Ember from 'ember';

export default Ember.Route.extend({
  beforeModel: function() {
    // route permissions?
  },

  model: function() {
    return [
    {
      name: "Joe",
      title: "Man at Arms",
      isActive: true 
    },
    {
      name: "Darah",
      title: "Spearman",
      isActive: false 
    },
    {
      name: "Daryl",
      title: "Crossbowman",
      isActive: true 
    }
    ];
  },

  afterModel: function() {
    // validate data?
  },
  
  actions: {
    addSoldier: function() {
      alert('handling action at route');
          
    }

  }
});
