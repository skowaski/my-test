import {
  moduleFor,
  test
} from 'ember-qunit';
import Ember from 'ember';

moduleFor('controller:index', {
  // Specify the other units that are required for this test.
  // needs: ['controller:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  var controller = this.subject();
  assert.ok(controller);
});

test('number of solders', function(assert) {
  var soldiers = [{
    
    isActive: true
  }];

  var controller = this.subject({model: soldiers});

  assert.equal(controller.get('numSoldiers'), 1, 'should have one soldier');
  assert.equal(controller.get('numActiveSoldiers'), 1, 'should have one active soldier');

  Ember.run(function() {
    controller.addObject({isActive: false});
  });
  
  assert.equal(controller.get('numSoldiers'), 2, 'should have two soldiers');
  assert.equal(controller.get('numActiveSoldiers'), 1, 'should have one active soldier');
});
